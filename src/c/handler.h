/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de
 
 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/

/*
 *  handler.h
 *  picflow
 *
 *  Created by Thomas Zink on 3/12/10.
 */
#ifndef _HANDLER_H_
#define _HANDLER_H_

#include <pcap.h>			// pcap packet capturing
#include "flow.h"

/* 
 handlers for the different layers
 we could do this all in one function actually
 and prevent function call overhead.
 however, this would decrease readability and maintainability
 greatly
 */
void ethernet_handler (u_char * args, const struct pcap_pkthdr *header, const u_char * packet);
void chdlc_handler (u_char * args, const struct pcap_pkthdr *header, const u_char * packet);
void ip_handler (u_char * args, const struct pcap_pkthdr *header, const u_char * ippacket);
void tcp_handler (u_char * args, const struct pcap_pkthdr *header, const u_char * ippacket);
void udp_handler (u_char * args, const struct pcap_pkthdr *header, const u_char * ippacket);
void update_stats (pic_connection_t conn, pic_flow_t flow);
void update_stats_print_flow (pic_connection_t conn, pic_flow_t flow);

#endif // _HANDLER_H_
