/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de
 
 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/

/*
 *  defines.h
 *  picflow
 *
 *  Created by Thomas Zink on 3/12/10.
 */

#ifndef _DEFINES_H_
#define _DEFINES_H_

#define APP_NAME		"picDFI"
#define AUTHOR			"Thomas Zink"
#define EMAIL			"thomas.zink@uni-konstanz.de"

/*
 DEBUG
 levels:
 -1 : serialize flows to stdout / will be replaced
 0 : no debug
 1 : packet level
 2 : flow level
*/
#define DEBUG -1

/*
 assert table integrity
 i.e. after each insert, find the element
 assert it != table.end()
 */
#define ASSERT 0


#define AGE_SERVICE_TABLE 1

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif

#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif

#endif // _DEFINES_H_
