###
# Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de
# 
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
__all__ = ['TYPE']

# +-------------------------------------------------------------+
# GLOBALS
# +-------------------------------------------------------------+

TYPE = {}
def _def_type(name,code):
    TYPE[name]=code
    TYPE[code]=name
_def_type('UNIDENTIFIED',0x00)
_def_type('POSSIBLE',0x01)
_def_type('P2P',0x02)
_def_type('NONP2P',0x04)

# +-------------------------------------------------------------+
# EOF
